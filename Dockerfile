FROM node:17

WORKDIR /app

COPY package*.json /app/

RUN npm install

RUN npm install pm2 -g

COPY . .

EXPOSE 3000

CMD ["npm", "start"]
